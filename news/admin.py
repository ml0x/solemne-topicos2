from django.contrib import admin
from news.models import Categoria, Noticia


@admin.register(Categoria)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('nombre','created_at','updated_at')
    pass

@admin.register(Noticia)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('titulo','autor', 'categoria','imagen_news','referencia', 'created_at','updated_at')
    pass